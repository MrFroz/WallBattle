package fr.froz;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.schematic.MCEditSchematicFormat;
import fr.froz.AriaCore.AriaAPI;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scoreboard.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/*All right reserved to Froz
* tous droits reservé a Froz
* Interdiction d'utlisation en dehors de l'accord
* explicite de Théo Matis */
public class Arena implements Listener {
    private Location loc, spawnPointRed, spawnPointBlue;
    private Player red, blue;
    private ArrayList<Player> playersList = new ArrayList<>();
    private int index;
    private int scoreRed = 0;
    private int scoreBlue = 0;
    private ScoreboardManager manager = Bukkit.getScoreboardManager();
    private Scoreboard board = manager.getNewScoreboard();
    private Objective objective = board.registerNewObjective("point", "dummy");
    private Score scoreB = objective.getScore(ChatColor.BLUE + "Bleu:");
    private Score scoreR = objective.getScore(ChatColor.RED + "Rouge:");

    Arena(Location l, int i) {
        loc = l;
        index = i;
        spawnPointBlue = new Location(Bukkit.getWorld("world"), l.getX() - 54.5, l.getY() + 12.3, l.getZ() + 6.5);
        spawnPointBlue.setYaw(-90);
        spawnPointRed = new Location(Bukkit.getWorld("world"), l.getX() - 0.5, l.getY() + 12.3, l.getZ() + 6.5);
        spawnPointRed.setYaw(90);
        pasteSchem(l);


    }

    private void sendTitle(String text, ChatColor color, Player p) {
        IChatBaseComponent chatTitle = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + text + "\",\"color\":\"" + color.name().toLowerCase() + "\"}");
        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, chatTitle);
        PacketPlayOutTitle length = new PacketPlayOutTitle(5, 20, 5);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(length);
    }


    private void pasteSchem(Location loc) {
        WorldEditPlugin we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
        File f = new File("plugins/WallBattle/arena/arena.schematic");
        @SuppressWarnings("deprecation")
        EditSession session = we.getWorldEdit().getEditSessionFactory().getEditSession(new BukkitWorld(loc.getWorld()), 1000000);
        try {
            MCEditSchematicFormat.getFormat(f).load(f).paste(session, new Vector(loc.getX(), loc.getY(), loc.getZ()), false);

        }
        catch (@SuppressWarnings("deprecation") MaxChangedBlocksException | com.sk89q.worldedit.data.DataException | IOException e2) {
            e2.printStackTrace();
        }
    }


    private boolean isPlayer(Player p) {
        return (red == p) || (blue == p);
    }

    private Location getLocation() {
        return loc;
    }

    private Location getRedSpawn() {
        return spawnPointRed;
    }

    private Location getBlueSpawn() {
        return spawnPointBlue;
    }

    private void setInv(Player p){
        p.getInventory().clear();
        ItemStack sword = new ItemStack(Material.IRON_SWORD);
        sword.addUnsafeEnchantment(Enchantment.DURABILITY,10);
        sword.addEnchantment(Enchantment.KNOCKBACK,1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
        ItemMeta swordMeta = sword.getItemMeta();
        swordMeta.setDisplayName(ChatColor.BOLD + "" + ChatColor.GOLD + "Aubéclat");
        ArrayList<String> lore = new <String>ArrayList<String>();
        lore.add(ChatColor.LIGHT_PURPLE + "Tu la veux mon épée ?");
        swordMeta.setLore(lore);
        sword.setItemMeta(swordMeta);
        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemMeta chestplateMeta = chestplate.getItemMeta();
        chestplateMeta.setDisplayName(ChatColor.GOLD + "Plastron Daedra");
        chestplate.setItemMeta(chestplateMeta);
        ItemStack legs = new ItemStack(Material.IRON_LEGGINGS, 1);
        legs.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
        ItemMeta legsMeta = legs.getItemMeta();
        legsMeta.setDisplayName(ChatColor.GOLD + "Bas Daedra");
        legs.setItemMeta(legsMeta);
        ItemStack boots = new ItemStack(Material.IRON_BOOTS, 1);
        boots.addEnchantment(Enchantment.PROTECTION_FALL, 1);
        ItemMeta bootsMeta = boots.getItemMeta();
        bootsMeta.setDisplayName(ChatColor.GOLD + "Bottes Daedra");
        boots.setItemMeta(bootsMeta);
        ItemStack pickaxe = new ItemStack(Material.IRON_PICKAXE);
        pickaxe.addEnchantment(Enchantment.DIG_SPEED,3);
        pickaxe.addUnsafeEnchantment(Enchantment.DURABILITY,10);
        ItemMeta pickaxeMeta = pickaxe.getItemMeta();
        pickaxeMeta.setDisplayName(ChatColor.DARK_BLUE + "The minator");
        pickaxe.setItemMeta(pickaxeMeta);
        ItemStack apple = new ItemStack(Material.GOLDEN_APPLE,16);
        ItemMeta appleMeta = apple.getItemMeta();
        appleMeta.setDisplayName(ChatColor.DARK_RED + "Apple Pen !");
        apple.setItemMeta(appleMeta);
        if(p==red){
            ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
            helmet.addUnsafeEnchantment(Enchantment.DURABILITY,10);
            LeatherArmorMeta helmetLetherMeta = (LeatherArmorMeta) helmet.getItemMeta();
            helmetLetherMeta.setColor(Color.RED);
            helmet.setItemMeta(helmetLetherMeta);
            p.getInventory().setItem(0, sword);
            p.getInventory().setHelmet(helmet);
            p.getInventory().setChestplate(chestplate);
            p.getInventory().setLeggings(legs);
            p.getInventory().setBoots(boots);
            p.getInventory().setItem(1,pickaxe);
            p.getInventory().setItem(2,new ItemStack(Material.SANDSTONE,64,(short)2));
            p.getInventory().setItem(3,apple);
        } else {
            ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
            helmet.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
            LeatherArmorMeta helmetLetherMeta = (LeatherArmorMeta) helmet.getItemMeta();
            helmetLetherMeta.setColor(Color.BLUE);
            helmet.setItemMeta(helmetLetherMeta);
            p.getInventory().setItem(0, sword);
            p.getInventory().setHelmet(helmet);
            p.getInventory().setChestplate(chestplate);
            p.getInventory().setLeggings(legs);
            p.getInventory().setBoots(boots);
            p.getInventory().setItem(1,pickaxe);
            p.getInventory().setItem(2,new ItemStack(Material.SANDSTONE,64,(short)2));
            p.getInventory().setItem(3,apple);
        }

    }
    void start() {
        Bukkit.getPluginManager().registerEvents(this, Bukkit.getPluginManager().getPlugin("wallsBattle"));
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.DARK_RED + "WallsBattle");
        scoreB.setScore(0);
        scoreR.setScore(0);
        red.setGameMode(GameMode.SURVIVAL);
        blue.setGameMode(GameMode.SURVIVAL);
        red.getInventory().clear();
        blue.getInventory().clear();
        setInv(red);
        setInv(blue);

        red.teleport(getRedSpawn());
        blue.teleport(getBlueSpawn());
        red.setScoreboard(board);
        blue.setScoreboard(board);
    }

    private void stop() {
        blue.setScoreboard(manager.getNewScoreboard());
        red.setScoreboard(manager.getNewScoreboard());
        AriaAPI.lobby(blue);
        AriaAPI.lobby(red);
        Main.unRegisterE(this);
        Main.freeArena[index] = true;


    }

    void addPlayer(Player p) {
        if (playersList.size() < 2) {
            playersList.add(p);
            if (red == null) {
                red = p;
            } else {
                blue = p;
            }
        }
    }
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        if (isPlayer(e.getPlayer())) {
            Main.freeArena[index] = true;
            if (e.getPlayer() == red) {
                AriaAPI.lobby(blue);
                Main.unRegisterE(this);
            } else {
                AriaAPI.lobby(red);
                Main.unRegisterE(this);
            }
        }
    }
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (isPlayer(e.getPlayer()) && e.getPlayer().getGameMode().equals(GameMode.SURVIVAL)) {
            Location playerLocation = e.getPlayer().getLocation();
            double x = playerLocation.getX();
            double y = playerLocation.getY();
            double z = playerLocation.getZ();
            if (y <= 0) {
                if (e.getPlayer() == red) {
                    e.getPlayer().setFallDistance(0F);
                    e.getPlayer().setGameMode(GameMode.SPECTATOR);
                    e.getPlayer().teleport(getRedSpawn());
                    e.getPlayer().setHealth(20);
                    red.sendMessage(ChatColor.BOLD + ""+ ChatColor.RED + "Vous etes mort :P");
                    blue.sendMessage(ChatColor.BOLD + "" + ChatColor.GREEN +e.getPlayer().getName()+ " est mort xD");
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), () -> {
                        e.getPlayer().setGameMode(GameMode.SURVIVAL);
                        e.getPlayer().teleport(getRedSpawn());
                    },(5*20));
                } else {
                    e.getPlayer().setFallDistance(0F);
                    e.getPlayer().setHealth(20);
                    e.getPlayer().setGameMode(GameMode.SPECTATOR);
                    e.getPlayer().teleport(getBlueSpawn());
                    blue.sendMessage(ChatColor.BOLD + ""+ ChatColor.RED + "Vous etes mort :P");
                    red.sendMessage(ChatColor.BOLD + "" + ChatColor.GREEN +e.getPlayer().getName()+ " est mort xD");
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), () -> {
                        e.getPlayer().setGameMode(GameMode.SURVIVAL);
                        e.getPlayer().teleport(getBlueSpawn());
                    },(5*20));
                }
            }
            if (e.getPlayer() == red) {
                if (y > 8 && y < 9.2 && z < this.getLocation().getZ() + 7.5 && z > this.getLocation().getZ() + 5.5 && this.getLocation().getX() - 53 > x && this.getLocation().getX() - 55 < x) {
                    red.setFallDistance(0F);
                    red.teleport(this.getRedSpawn());
                    red.setHealth(20);
                    blue.setFallDistance(0F);
                    blue.teleport(this.getBlueSpawn());
                    blue.setHealth(20);
                    scoreRed++;
                    scoreR.setScore(scoreRed);
                    red.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.RED + " Le rouge marque 1 point !" + ChatColor.WHITE + " (" + ChatColor.WHITE + scoreRed + "/5)");
                    blue.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.RED + " Le rouge marque 1 point !" + ChatColor.WHITE + " (" + ChatColor.WHITE + scoreRed + "/5)");
                    if (scoreRed >= 5) {
                        red.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BOLD + "" + ChatColor.WHITE + ChatColor.RED + "Le joueur rouge remporte la victoire !");
                        blue.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "] " + ChatColor.RED + "Le joueur rouge remporte la victoire !");
                        sendTitle("GG le rouge xD", ChatColor.RED, red);
                        sendTitle("GG le rouge xD", ChatColor.RED, blue);

                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), this::stop, (5 * 20));
                    }

                }
            } else if (y > 8 && y < 9.2 && z < this.getLocation().getZ() + 7.5 && z > this.getLocation().getZ() + 5.5 && this.getLocation().getX() - 0 > x && this.getLocation().getX() - 2 < x) {
                red.setFallDistance(0F);
                red.teleport(this.getRedSpawn());
                red.setHealth(20);
                blue.setFallDistance(0F);
                blue.teleport(this.getBlueSpawn());
                blue.setHealth(20);
                scoreBlue++;
                scoreB.setScore(scoreBlue);
                red.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BLUE + " Le bleu marque 1 point !" + ChatColor.WHITE + " (" + ChatColor.WHITE + scoreBlue + "/5)");
                blue.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BLUE + " Le bleu marque 1 point !" + ChatColor.WHITE + " (" + ChatColor.WHITE + scoreBlue + "/5)");
                if (scoreBlue >= 5) {
                    red.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BLUE + " Le joueur bleu remporte la victoire !");
                    blue.sendMessage(ChatColor.BOLD + "" + ChatColor.WHITE + "[" + ChatColor.DARK_AQUA + "AriaGame" + ChatColor.WHITE + "]" + ChatColor.BLUE + " Le joueur bleu remporte la victoire !");
                    sendTitle("GG le bleu xD", ChatColor.BLUE, red);
                    sendTitle("GG le bleu xD", ChatColor.BLUE, blue);
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), this::stop, (5 * 20));
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (isPlayer(e.getPlayer())) {
            if (!(e.getBlock().getType() == Material.SANDSTONE)) {
                e.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        if(isPlayer(e.getPlayer())){
            if(e.getBlock().getType()==Material.SANDSTONE){
                if (e.getPlayer().getInventory().getItemInMainHand().getType() == Material.SANDSTONE) {
                    e.getPlayer().getInventory().getItemInMainHand().setAmount(65);
                } else {
                    e.getPlayer().getInventory().getItemInOffHand().setAmount(65);
                }
            }
        }
    }
    @EventHandler
    public void onPlayerdamage(EntityDamageEvent e){
        if ((e.getEntity() instanceof Player)){
            Player p = (Player) e.getEntity();
            if(isPlayer(p)&&(p.getHealth()-e.getDamage()<1)){
                e.setCancelled(true);
                p.setHealth(20);
                p.setFallDistance(0F);
                p.setGameMode(GameMode.SPECTATOR);
                if(p==red){
                    p.teleport(this.getRedSpawn());
                    red.sendMessage(ChatColor.BOLD + ""+ ChatColor.RED + "Vous etes mort :P");
                    blue.sendMessage(ChatColor.BOLD + "" + ChatColor.GREEN +p.getName()+ " est mort xD");
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), () -> {
                        p.setGameMode(GameMode.SURVIVAL);
                        p.teleport(getRedSpawn());
                    },(5*20));
                } else {
                    p.teleport(this.getBlueSpawn());
                    blue.sendMessage(ChatColor.BOLD + ""+ ChatColor.RED + "Vous etes mort :P");
                    red.sendMessage(ChatColor.BOLD + "" + ChatColor.GREEN +p.getName()+ " est mort xD");
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("wallsBattle"), () -> {
                        p.setGameMode(GameMode.SURVIVAL);
                        p.teleport(getBlueSpawn());
                    },(5*20));
                }

            }
        }
    }

}
